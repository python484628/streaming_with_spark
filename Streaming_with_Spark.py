

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021

# Install pyspark if it's necessary
!pip install pyspark

# Import libraries
from operator import add
from time import sleep
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

# Set up the Spark context and the straming context
sc = SparkContext(appName="PysparkStreaming")
ssc = StreamingContext(sc, 1) # 1 for each second to retrieve data

"""- Create some input data and push them in the streaming queue"""

# Input data
rddQueue = []
for i in range(10): # List until 10 and we collect fictive data
  rddQueue += [ssc.sparkContext.parallelize([i, i+1])]

inputStream = ssc.queueStream(rddQueue)



"""- Perform some processing of the data"""

inputStream.map(lambda x: "Input: " + str(x)).pprint() # pprint for structured data for ex JSON (instead of having one line with a lot of info it will be structured)
s=inputStream.reduce(add).map(lambda x: "Output: " + str(x)).pprint() # We print the output the sum

"""- Start streaming"""

ssc.start()

"""- The streaming continues until you stop it"""

ssc.stop(stopSparkContext=True, stopGraceFully=True)

"""#Spark Streaming example 2"""

# Import libraries
from operator import add
from time import sleep
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

# Set up the Spark context and the streaming context
sc = SparkContext(appName="pyspark_Streaming")
ssc = StreamingContext(sc, 1) # 1 for each second to retrieve data
inputData = [ [1,2,3], [0], [4,4,4], [0,0,0,25], [1,-1,10] ]
rddQueue = []
for datum in inputData:
  rddQueue += [ssc.sparkContext.parallelize(datum)]

inputStream=ssc.queueStream(rddQueue)
inputStream.reduce(add).pprint()

ssc.start()
sleep(5)
ssc.stop(stopSparkContext=True, stopGraceFully=True)

# Import library
from pyspark.sql import SparkSession
# Create Spark session
spark = SparkSession.builder.appName("StructuredStreaming").getOrCreate()

"""- Streaming the dataset"""

from pyspark.sql.types import TimestampType, StringType, StructType, StructField

# Path to our 20 JSON files
#inputPath = "spark-streaming-sample-data/" # sample_data/
inputPath = "sample_data/" # sample_data/
# Explicitly set the schema
schema = StructType([ StructField("time", TimestampType(), True),
                     StructField("customer", StringType(), True),
                     StructField("action", StringType(), True),
                     StructField("device", StringType(), True)])

"""- Create a streaming DF using .readStream"""

streamingDF = spark.readStream.schema(schema).option("maxFilesPerTrigger", 1).json(inputPath)

from google.colab import drive
drive.mount('/content/drive')

"""- Check if it is streaming"""

streamingDF.isStreaming

"""- We can write the stream in the console"""

query = streamingDF.writeStream.format("console").start()

query.stop()

"""- Or export to disk (Other formats (kafka,....) are also available, check the doc)"""

path= "spark-streaming-sample-data" # sample_data
query = streamingDF.writeStream.format("csv").option("checkpointLocation",path+"/checkpoints").option("path", path+"/output").queryName("counts").outputMode("append").start() # spark-streaming-sample-data/

query.stop()

"""- Or do some processing on the stream"""

# Stream streamingDF while aggregating by action
streamingActionCountsDF = streamingDF.groupBy(streamingDF.action).count()

# Is streamingActionCountsDF actually streaming ?
streamingActionCountsDF.isStreaming

"""- Check the stream in the console"""

# writing the streaming somewhere (in console here) in real-time
query = streamingActionCountsDF \
.writeStream \
.format("console") \
.queryName("countsDF") \
.outputMode("complete") \
.start()

query.stop()

"""- The end, stop spark"""

spark.stop()
